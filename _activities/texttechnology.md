---
layout: page
title: Texttechnologie
feature-img: "assets/img/sigs/code.jpg"
permalink: /activities/texttechnology/
tags: [SIG]
lang: de
---
Leitung: [Roman Schneider](mailto:schneider@ids-mannheim.de), [Gertrud Faaß](mailto:faassg@uni-hildesheim.de) 

Der AK Texttechnologie befasst sich vorrangig mit der Integration von Standards der Textstrukturierung (Markup Languages) und der linguistischen Datenverarbeitung. Ziel ist es, dadurch die Entwicklung innovativer Textmodelle und inhaltsorientierter Textverarbeitung und -nutzung zu ermöglichen. Das Hauptaugenmerk richtet sich auf die Verarbeitung deutschsprachiger Texte und innovativer Textsorten (z.B. [Songtexte](http://songkorpus.de/)); der Anspruch bezieht sich auch auf Sprachvarietäten, Sozio- und Regiolekte und die kontrastive Untersuchung bisher wenig erforschter Sprachen, was ggf. eine Überprüfung und Erweiterung existierender Standards erfordert.

Für die Erkennung von Unterschieden zwischen diesen Sprachen und Sprachvarianten und bereits gut untersuchten Sprachen, d.h. für kontrastive Forschung, ist vordererst die Kompilation passender Datenbestände (Korpora) nötig. Hier kümmert sich der Arbeitskreis u.a. um die Definition relevanter Metadatenkategorien und unterstützt die Erstellung und Dokumentation einschlägiger Goldstandards.

# JLCL Vol. 36 (1)

Die Sonderausgabe des Journal of Language Technology and Computational Linguistics, JLCL 36(1) mit dem Thema "Computerlinguistische Herausforderungen, empirische Erforschung & multidisziplinäres Potenzial deutschsprachiger Songtexte" (herausgegeben von Roman Schneider und Gertrud Faaß) ist online und [hier](https://jlcl.org/issue/view/62/61) abrufbar.
