---
layout: page
title: CPSS Workshop @ KONVENS 2023
feature-img: "assets/img/events/classroom.jpg"
img: "assets/img/events/classroom.jpg"
lang: de
---


Im Rahmen der KONVENS Konferenz in Ingolstadt fand am 22.September 2023 der Workshop "Computational Linguistics for the Political and Social Sciences (CPSS)" statt. Der Workshop wurde von Christopher Klamm (Uni Mannheim), Valentin Gold (Uni Göttingen), Gabriella Lapesa (Uni Stuttgart), Simone Ponzetto (Uni Mannheim) und Theresa Gessler (European University Viadrina) organisiert.

Der Workshop beinhaltete 21 Präsentationen, die sich auf einzelne Vorträge und zwei Postersitzungen verteilten. Insgesamt nahmen etwa 40 Teilnehmer und Teilnehmerinnen aus den Bereichen Natürliche Sprachverarbeitung (NLP) und den Sozial-/Politikwissenschaften am Workshop teil. Die Veranstaltung war äußerst erfolgreich, und die großen Postersitzungen trugen besonders zum interdisziplinären Austausch bei. Die beiden eingeladenen Vorträge von Lucie Flek (Uni Bonn) zum Thema "On "fixing" the framing: Is bias detection a pair-wise comparison task?" und Sebastian Pado (Uni Stuttgart) zum Thema "Computational construction of discourse networks for political debates" boten weitere Möglichkeiten, die vielfältigen Potenziale und Herausforderungen von NLP-Verfahren in den Sozial- bzw. Politikwissenschaften zu diskutieren.

Der CPSS Workshop war eine fantastische Veranstaltung, die einen hervorragenden Ausgangspunkt für künftige Forschungskooperationen bot. Wir sind dankbar für das positive, unterstützende Umfeld, das die Teilnehmer und Teilnehmerinnen geschaffen haben, und wir könnten nicht zufriedener sein mit der hohen Teilnehmerzahl und dem aktiven Engagement während der gesamten Veranstaltung. Ein großes Dankeschön an alle, die dazu beigetragen haben, dass wir eine so großartige und kreative Gemeinschaft bilden konnten. Wir können es kaum erwarten, zu sehen, was als nächstes kommt!

Das vollständige Programm sowie die Zusammenfassungen und Folien der eingeladenen Vorträge finden Sie unter: https://sites.google.com/view/cpss2023konvens/home-page


 <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_01.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_04.jpg' | relative_url }}">
 </div>
 
 <div style="clear:both;"></div>
