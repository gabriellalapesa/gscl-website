---
layout: page
title: CPSS Workshop @ KONVENS 2023
feature-img: "assets/img/events/classroom.jpg"
img: "assets/img/events/classroom.jpg"
lang: en
---


The 3rd edition of the Workshop on Computational Linguistics for the Political and Social Sciences (CPSS) took place on the 22 of September 2023, co-located with KONVENS 2023 in Ingolstadt. It has been organized by Christopher Klamm (U. Mannheim), Valentin Gold (U. Göttingen), Gabriella Lapesa (U. Stuttgart),Simone Ponzetto (U. Mannheim), and Theresa Gessler (European U. Viadrina).

The workshop featured 21 presentations, distributed over one oral and two poster sessions, and it was attended by approximately 40 participants from both Natural Language Processing and the Social/Political sciences. The event was extremely successful, and the large poster sessions were particularly effective in promoting interdisciplinary exchange. The two invited talks by Lucie Flek (U. Bonn) on “On "fixing" the framing: Is bias detection a pair-wise comparison task?” and Sebastian Pado (U. Stuttgart) on “Computational construction of discourse networks for political debates” provided further input to the discussion by showcasing the interface of NLP with the social sciences and political science, respectively.

Overall, it was a fantastic event and provided an amazing starting point for future research collaborations. We were so grateful for the positive, supportive environment created by the participants, and we could not have been happier with the high number of participants and their active engagement throughout the event. Big thanks to all of you who made this happen and came together to form such an awesome and creative community. We can not wait to see what is next!

The full program and the abstracts and slides of the invited talk can be accessed at: https://sites.google.com/view/cpss2023konvens/home-page



 <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_01.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/cpss_2023_04.jpg' | relative_url }}">
 </div>
 
 <div style="clear:both;"></div>
