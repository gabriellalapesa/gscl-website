---
layout: page
title: KONVENS Teach4NLP Workshop @ KONVENS 2023
feature-img: "assets/img/events/classroom.jpg"
img: "assets/img/events/classroom.jpg"
lang: en
---

# KONVENS Teaching for NLP Workshop

## Workshop Date & Venue
* September 18, 12pm - 6pm + Workshop Dinner
* TH Ingolstadt
* Co-located with KONVENS 2023

* Submission Deadline: June 9, 2023
* Notification: July 5, 2023
* Camera Ready Deadline: July 15, 2023

## Contact
konvens-teach4nlp@googlegroups.com

## Aim of the Workshop

The fast-paced nature of progress in NLP poses unique challenges to educators engaging in curriculum design for NLP courses and NLP-related degree programs. Its rapid growth has led to the creation and revision of thousands of courses and degree programs at universities and online, as well as new educational materials focused on emerging subareas of NLP (e.g., prompting,  ethics in NLP systems). For the Workshop on Teaching NLP we propose to bring the NLP community together at KONVENS 2023 to discuss the following topics:
* What are key elements of NLP and/or CL curricula?
* What adaptations are necessary to cater to different groups (i.e. core computer science students vs. core CL students vs. Digital Humanities students)
* What topics should be covered for students studying NLP/CL as major subject, which topics should be covered for students studying NLP/CL as minor subjects?
* What is unique about teaching NLP/CL in German speaking contexts?
* What challenges do we face when working with German or other non-English languages as a language, data sets, etc.?
* What are key differences between regular universities and universities of applied sciences?

We invite submissions relating to teaching CL/NLP in applied linguistics, digital humanities, computational linguistics, data science, or computer science.


## Call for Contributions

One of our major goals is to provide resources for teachers and instructors that can last beyond the discussions held on the day of the workshop. We invite 2-3 page short papers or 6-8 page long papers (with unlimited pages of references) that can be **position papers on teaching methodology and theory** or **teaching materials** (lecture slides, exercises, project descriptions) with 2-3 page short papers describing the materials.

* Tools and methodologies (e.g. teaching with code, active learning, flipped classroom)
* Adapting existing curriculum to incorporate new NLP advancements
* Choice of topics to cover for different target audiences
* Developing courses for (IT) students without linguistics background
* Developing courses for students who have non-computer science backgrounds
* Teaching NLP in German speaking context (or using other languages than English in general)
* Position of NLP in curricula at universities of applied sciences
* Ethics, reproducibility, and responsible practices
 
Existing resources for teaching NLP are often scattered across course and faculty web pages or they are outdated, such as the ACL Wiki's page on Teaching. We plan to join forces with the ACL-wide Teaching NLP workshop to add resources to their repository in order to make our material more generally available.

Submission will be single-blind (i.e., they should not be anonymous) and follow the [ACL style guidelines](https://github.com/acl-org/acl-style-files).

Submission must be electronic via the [KONVENS CMT System](https://cmt3.research.microsoft.com/KONVENS2023/Submission/Index). Select the Teach4NLP Workshop track during submission. After submitting your paper, you can submit attachments in a second step.


## Preliminary Program

The workshop will feature:
* An informal round of introductions.
* A poster session for paper/teaching materials submissions.
* A discussion round on "What should we teach and why?". What knowledge and skills do NLP/CL graduates need?
* A discussion round on formats for / sharing of teaching materials.
* A workshop dinner for further informal exchange.



## Organizers
* [Margot Mieskes](https://sis.h-da.de/personen/professor-innen-auf-einen-blick/prof-dr-margot-mieskes)  is a Professor for Information Science at the University of Applied Sciences, Darmstadt. She has studied computational linguistics in Stuttgart, Edinburgh, and Cambridge, and did her PhD in Heidelberg. She is also a co-organizer of the ACL-wide Teaching for NLP workshop.
* [Jannik Strötgen](https://sites.google.com/view/jannikstroetgen/home?pli=1) is a faculty member of FH Karlsruhe. He holds a PhD from the University of Heidelberg.
* [Christian Wartena](https://im.f3.hs-hannover.de/studium/personen/prof-dr-christian-wartena/) holds a PhD from the University of Potsdam. He worked for several companies in Germany and the Netherlands on machine translation, morphological analysis, keyword extraction and recommender systems. Since 2011 he is professor for knowledge and language processing at the university of applied sciences and arts in Hannover, where he teaches courses on natural language processing, text mining, information retrieval and semantic web in the Information Management program. He also regularly teaches NLP courses at the University of Hildesheim.
* [Annemarie Friedrich](https://annefried.github.io/) is a Professor for Natural Language Understanding at the Faculty of Applied Computer Science at the University of Augsburg, and the Vice President of the GSCL. She holds a PhD in Computational Linguistics from Saarland University and is highly interested in activation concepts in university teaching.
* [Stefan Grünewald](https://stgrue.net/) is a PhD student working at Stuttgart University and the Bosch Center for Artificial Intelligence. He is a member of the Enabling team at Bosch, teaching machine learning to Bosch employees with a wide range of job roles. His research interests include syntax (specifically syntactic dependencies) and neural architectures for structured prediction in NLP. Furthermore, his responsibilities within Bosch involve teaching NLP and ML concepts to non-experts.


## Program Committee

* Margot Mieskes
* Jannik Strötgen
* Christian Wartena
* Annemarie Friedrich
* Stefan Grünewald
* Torsten Zesch
* Barbara Plank
* Cerstin Mahlow
* Ines Rehbein
* Alessandra Zarcone
* Heike Adel
* Nils Reiter
* Anette Frank
* Heike Adel
* Jakob Prange
* Ulrike Padó
