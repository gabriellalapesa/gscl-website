---
layout: post
title: Dialogue and Robots for Understanding Understanding
gscltalk: true
speaker: Casey Redd Kennington
organization: Boise State University
speakerimage: "assets/img/people/talks/casey.jpg"
speakerimageteaser: "assets/img/people/talks/casey-300x225.jpg"
time: "16:00"
tags: [talks]
lang: en
---

Hallmarks of intelligence include the ability to acquire, represent, understand, and produce natural language. Although recent efforts in data-driven, machine learning, and deep learning methods have advanced natural language processing applications, important challenges remain. In my talk, I'll give an overview of general trends in understanding language on machines, what we can learn from children who acquire language seemingly with minimal effort, and what that means for future research. I will then explain my own research on grounding language into different physical modalities, what role emotion could play, and the potential importance of embodiment.

# Biography

Department of Computer Science, Boise State University. He completed his PhD in Linguistics at Bielefeld University in Germany and his masters degrees in Computational Linguistics and Cognitive Science, respectively, from Saarland University in Saarbrücken, Germany, and Nancy 2 University in Nancy, France. His research at Boise State University brings together computer science, machine/deep learning, human-robot interaction, natural language processing, spoken dialogue systems, child development, and cognitive science. [caseyreddkennington.com](https://www.caseyreddkennington.com/)
