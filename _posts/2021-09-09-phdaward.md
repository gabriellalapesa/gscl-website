---
layout: post
title: Promotionspreis
tags: [award]
lang: de
---

Alle zwei Jahre vergibt die GSCL zwei mit jeweils 400 Euro dotierte Preise für die beste studentische Bachelor-Abschlussarbeit sowie für die beste Master-Arbeit auf dem Gebiet der Sprachtechnologie und Computerlinguistik. Preisträger in der Kategorie Bachelor für die Auswahlrunde 2019–2021 ist Yannic Bracke (Universität Potsdam) für seine Arbeit und Präsentation zum Thema „Automatic text classification with imbalanced data – Building a frame classifier from a corpus of editorials“. In der Kategorie Master gewann Marie Bexte (Universität Duisburg-Essen), deren Arbeit den Titel „Combined Analysis of Image and Text Using Visio-Linguistic Neural Models – A Case Study on Robustness Within an Educational Scoring Task“ trägt. Wir gratulieren den Preisträger*innen herzlich zu ihren hervorragenden Arbeiten.