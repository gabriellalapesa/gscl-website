---
layout: post
title: Klausurtagung Vorstand und Beirat in Fulda 2022
feature-img: "assets/img/news/vb2022_fulda.JPG"
thumbnail: "assets/img/news/vb2022_fulda_thumb.JPG"
tags: [meetings]
lang: de
---

Nach 2 Jahren virtueller Zusammenarbeit fand vom 2.-3. September in Fulda eine gemeinsame Klausurtagung des Vorstands und Beirats der GSCL statt.
Im Mittelpunkt standen mehrere Arbeitssitzungen, in denen unter anderem die Gestaltung der GSCL Arbeitskreise, neue Konzepte für die KONVENS, die Kommunikation innerhalb und außerhalb der GSCL, Ideen für die Einbeziehung der Studierendenmitglieder, sowie die neu gestaltete GSCL Webseite und Pläne für eine Neugestaltung des Portal Computerlinguistik diskutiert wurden.
Christian Wartena, der seit 2021 Herausgeber ist, leitete eine Diskussion zur Zukunft des von der GSCL herausgegebenen Journals JLCL.
Abgerundet wurde das Programm durch eine Dombesichtigung und ein gemeinsames Abendessen.

<div style="float:left; width: 320px; min-height: 300px; margin-left: 10px">
   <img src="{{ '/assets/img/news/vb2022_fulda_1.JPG' | relative_url }}">
    Nicolai Erbs präsentiert die neue Webseite.
 </div>
 <div style="float:left; width: 400px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/news/vb2022_fulda_2.JPG' | relative_url }}">
	Vorstand und Beirat der GSCL im Tagungsraum des Brauhauses Wiesenmühle in Fulda.
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/news/vb2022_fulda_3.JPG' | relative_url }}">
    Metaplan-Wand.
 </div>
 
 <div style="clear:both;"></div>
