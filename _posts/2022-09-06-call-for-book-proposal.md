---
layout: post
title: Ausschreibung - Förderung einer Open-Access-Publikation „Einführung in die Computerlinguistik“
tags: [book_proposal open_access textbook]
lang: de
---

Aufforderung zur Konzepteinreichung

**Zum Hintergrund**

Das letzte Jahrzehnt hat weltweit zahlreiche neue Aktivitäten und Entwicklungen auf
dem Feld der Computerlinguistik erbracht. Nicht nur haben sich neue Ansätze und
Methoden durchgesetzt; die Computerlinguistik als solche hat sich auch disziplinär
stark ausdifferenziert. Dessen ungeachtet ist festzustellen, dass für Forschung und
Lehre kein aktuelles deutschsprachiges Lehrbuch zur Verfügung steht, das ein
umfassendes Bild des aktuellen Standes der Computerlinguistik zu zeichnen vermag.
Die GSCL möchte deshalb dazu beitragen, diese Lücke zu füllen und hat beschlossen,
Mittel zur Veröffentlichung eines umfassenden einführenden Lehrbuches oder einander
ergänzender Lehrbücher zur Verfügung stellen.

**Was wird gefördert?**

Die Einführung soll sich vorrangig an den wissenschaftlichen Nachwuchs, insbesondere
an Studierende in einem Bachelor-Studiengang, richten. Diese allgemeine Einführung in
den Forschungsgegenstand sollte zudem die Trends der letzten Jahre thematisieren.
Der Fokus der Publikation oder der Publikationen könnte beispielsweise auf
anwendungsorientierte Fragestellungen der Informatik gelegt werden. Alternativ
könnten auch Schwerpunktsetzungen in den Bereichen Linguistik oder Digital
Humanities erfolgen.

Die GSCL wird die finanziellen Mittel bereitstellen, die zur Deckung der
Publikationskosten eines oder mehrerer sich ergänzender deutschsprachiger Open-
Access-Bände zur Einführung in die Computerlinguistik in einem renommierten Verlag
benötigt werden. Sofern die Autorinnen und Autoren bzw. Herausgeberinnen und
Herausgeber die Publikation bei einem kostenfreien Non-Profit-Verlag bevorzugen,
unterstützt die GSCL auch solche Vorhaben, kann die dadurch freiwerdenden
finanziellen Mittel allerdings nicht umwidmen.

Momentan steht die GSCL mit renommierten Verlagen in Kontakt, die Autorinnen und
Autoren nicht nur ein breitgefächertes Distributionsnetz, sondern auch deutliche
Mehrwerte für die Sichtbarkeit und Reichweite ihrer Forschung bieten. Die Einführung
soll so konzipiert werden, dass sie sowohl in digitaler Fassung als PDF über Bibliotheken
und Gedächtnisorganisationen barrierefrei zur Verfügung steht, als auch als
Druckversion über den Buchhandel verbreitet und von Bibliotheken angeschafft werden
kann.

Die GSCL wird alle mit der Publikation anfallenden Kosten von professionellem Lektorat,
Satz und dauerhafter Bereitstellung des Bandes übernehmen. Es steht zu erwarten,
dass ein Zuschlag für die erfolgreichen Autorinnen und Autoren erhebliche
Reputationsgewinne infolge der Schließung einer Lücke in der deutschsprachigen
Computerlinguistik zur Folge haben wird. Weitere Anreize, insbesondere Honorare für
das Verfassen von Texten oder die Herausgabe des Bandes werden von der GSCL
explizit nicht ausgelobt.

**Zur Antragstellung**

Die GSCL bittet einschlägig erfahrene Wissenschaftlerinnen und Wissenschaftler um
Einreichung eines Essays zur thematischen Ausrichtung einer Einführung. Die
Einführung kann sowohl als monographische Schrift als auch als Sammelband
konzipiert werden.

Bis zum <span style="color:blue">15.12.2022</span> sind Autorinnen und Autoren oder Herausgeberinnen und
Herausgeber aufgerufen, eine entsprechende kurze Konzeption einzureichen, welche
die thematische Ausrichtung des Lehrbuches konzise skizziert und, im Falle von
Sammelbänden, Einzelthemen und potenzielle Autorinnen und Autoren aufführt.
Erwartet wird überdies ein straffer, aber umsetzbarer Zeitplan zur Durchführung der
Publikation, die idealerweise innerhalb eines Jahres abzuschließen sein wird. Die
Auswahl der Einreichungen wird bis zum <span style="color:blue">31.01.2023</span> erfolgen; entsprechende Vorschläge
sind bitte unter dem Betreff „GSCL: Einführung in die Computerlinguistik“ an die
Adresse informationsreferent@gscl.org zu richten.
