---
layout: post
title: GSCL Reisestipendien KONVENS 2022
tags: [student awards]
thumbnail: "assets/img/news/GSCL-KONVENS2022-Stipendiaten.jpg"
lang: de
---


In diesem Jahr wurden drei Reisestipendien für BA-/MA-Studierende mit eigenen Beiträgen zu KONVENS vergeben (in alphabetischer Reihenfolge): Jonathan Baum (Hochschule Darmstadt), Ginevra Martinelli (Università Cattolica del Sacro Cuore Largo Gemelli, 1 - 20123 Mila) und Thorben Schomaker (Hochschule für Angewandte Wissenschaften Hamburg), die alle ihre Poster erfolgreich vor vielen interessierten Konferenzteilnehmern präsentierten.
Die GSCL ist stolz darauf, ihre studentischen Mitglieder zu unterstützen, die Beiträge zu Computerlinguistik-Konferenzen leisten.
Darüber hinaus können sich auch BA-/MA-Studenten ohne akzeptierte Papiere oder Poster für den Besuch solcher Konferenzen bewerben, um an den Diskussionen teilzunehmen und ein Mitglied unserer Gemeinschaft zu werden.


<div style="float:left; width: 600px; min-height: 300px; margin-left: 10px">
    <img src="{{ '/assets/img/news/GSCL-KONVENS2022-Stipendiaten.jpg' | relative_url }}">
    Preisträger*innen des GSCL Reisestipendiums für die KONVENS 2022.
 </div> 
 <div style="clear:both;"></div>
