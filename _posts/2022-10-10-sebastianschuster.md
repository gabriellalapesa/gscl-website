---
layout: post
title: When a sentence does not introduce a discourse entity, Transformer-based models still sometimes refer to it
gscltalk: true
speaker: Sebastian Schuster
organization: Universität des Saarlandes
speakerimage: "assets/img/people/talks/sebastianschuster.jpg"
speakerimageteaser: "assets/img/people/talks/sebastianschuster-300x225.jpg"
tags: [talks]
time: "16:00"
lang: en
---

Understanding longer narratives or participating in conversations requires tracking of discourse entities that have been mentioned. Indefinite noun phrases (NPs), such as ‘a dog’, frequently introduce discourse entities but this behavior is modulated by sentential operators such as negation. For example, ‘a dog’ in ‘Arthur doesn’t own a dog’ does not introduce a discourse entity due to the presence of negation. In this work, we adapt the psycholinguistic assessment of language models paradigm to higher-level linguistic phenomena and introduce an English evaluation suite that targets the knowledge of the interactions between sentential operators and indefinite NPs. We use this evaluation suite for a fine-grained investigation of the entity tracking abilities of the Transformer-based models GPT-2 and GPT-3. We find that while the models are to a certain extent sensitive to the interactions we investigate, they are all challenged by the presence of multiple NPs and their behavior is not systematic, which suggests that even models at the scale of GPT-3 do not fully acquire basic entity tracking abilities.


<div style="float:left; width: 320px; min-height: 300px; margin-left: 10px">
   <img src="{{ page.speakerimage | relative_url }}">
</div>

# Biography
Sebastian Schuster is currently a Postdoctoral Researcher at Saarland University as part of Vera Demberg's group. His research focuses on developing and evaluating models of language understanding. He was previously a Postdoctoral Fellow at the Center of Data Science and the Department of Linguistics at NYU, and he holds an MS degree in Computer Science and a PhD in Linguistics from Stanford University, and a BS in Computer Science from the University of Vienna.

 
