---
layout: post
title: GSCL 2023 BA and MA Thesis Awards
tags: [ba_ma_thesis_award]
lang: en
---

In line with its mission to promote research, teaching and professional work in natural language processing and computational linguistics, GSCL bi-annually awards two prizes worth € 400 each for the best student undergraduate thesis and for the best master’s thesis.

* Eligible theses for this year's award are ones submitted between April 2021 and March 2023. 
* Theses from all German-speaking countries will be considered (Austria, Germany and Switzerland) as well as from any other country, as long as the topic is focused on the German language.
* The current selection round takes place in the summer of 2023, with the final selection to be made as part of the KONVENS conference in Ingolstadt (September 18-22, 2023). 
* Candidates for the awards can be put forward by thesis supervisors. 
* Nominations must be emailed to `gscl-preis@gscl.org` no later than June 15, 2023.

For more information on how to apply please visit [https://gscl.org/en/activities/studentaward](https://gscl.org/en/activities/studentaward).
