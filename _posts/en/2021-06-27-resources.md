---
layout: post
title: Ethics Workshop
tags: [resources]
lang: en
---

Under the link [Resources](/resources) materials are now available that were developed following up the GSCL Ethics Workshop 2020. The collection contains teaching materials on the subject of ethics in natural language processing as well as a questionnaire designed to help the future organizers of GermEval Shared Tasks.