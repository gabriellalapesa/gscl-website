---
layout: post
title: GSCL Travel Scholarships KONVENS 2022
tags: [student award]
thumbnail: "assets/img/news/GSCL-KONVENS2022-Stipendiaten.jpg"
lang: en
---

Three travel and accommodation grants for BA-/MA-students with own contributions to the KONVENS were awarded this year (in alphabetical order): Jonathan Baum (Unversity of Applied Science, Darmstadt), Ginevra Martinelli (Università Cattolica del Sacro Cuore Largo Gemelli, 1 - 20123 Mila), and Thorben Schomaker (Hamburg Universtiy of Applied Sciences), who all successfully presented their posters to many interested conference participants.
GSCL is proud to support its student members who contribute to computational linguistics conferences.
In addition, BA-/MA-students without accepted papers or posters can also apply to visit such conferences in order to meet, take part in the discussions and to become a member of our community.


<div style="float:left; width: 600px; min-height: 300px; margin-left: 10px">
    <img src="{{ '/assets/img/news/GSCL-KONVENS2022-Stipendiaten.jpg' | relative_url }}">
Awardees of GSCL Travel Scholarships 2022.
 </div> 
 <div style="clear:both;"></div>
