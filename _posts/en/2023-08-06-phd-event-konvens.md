---
layout: post
title: PhD Event at KONVENS 2023 in Ingolstadt
tags: [konvens2023]
lang: en
---

We would like to invite all PhD students in the field of Natural Language Processing (or adjacent) to join the PhD Event at KONVENS 2023 @ Technische Hochschule Ingolstadt.

The conference itself provides the ideal opportunity to come into contact with work on Computational Linguistics in both academia and industry. The PhD Event is designed to offer a space for (fun) discussions relating to all aspects of life as a PhD student in NLP and to get to know your community.

PhD students have the opportunity to briefly present their topic in 3-4 minutes lightning talks with a chance to get feedback and exchange ideas with their peers.

We will also have a session showing how applying creativity can help coping with difficulties and with managing insecurities.
The session will consist of:

* Art-therapy warm-up: Expression of emotions through colour
* Main activity: Comic drawing and story telling
* Art-therapy windup: Transformation of unpleasant experiences

We're excited to have you join us for a fun and informative event!
More information here: https://www.thi.de/konvens-2023/


