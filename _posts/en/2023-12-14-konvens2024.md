---
layout: post
title: Save the date - KONVENS 2024
tags: [konvens2024]
lang: en
---

A ray of hope behind the wintry horizon: KONVENS 2024 will take place in Vienna from September 9th to 13th, 2024, organized by [ASAI](https://www.asai.ac.at/). In addition to the main conference, there will be events such as workshops, a sexism classification challenge for German, a GSCL meeting and much more.


<!--<div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_01.jpg' | relative_url }}">
 </div>-->
<!--<div style="float:left; width: 480px; min-height: 270px; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens2024promo.png' | relative_url }}">
</div>-->
<!--<div style="float:left; width: 480px; min-height: 270px; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens24promo.jpg' | relative_url }}">
</div>-->
<div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens24promo.png' | relative_url }}">
</div>


 
<!--   <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_04.jpg' | relative_url }}">
 </div>
 --> 
 <div style="clear:both;"></div>
