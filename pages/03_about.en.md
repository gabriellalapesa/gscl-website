---
layout: page
title: About the GSCL
permalink: /about/
feature-img: "assets/img/aboutus.jpg"
tags: [Page]
lang: en
---




The German Society for Computational Linguistics and Language Technology (GSCL) is the scientific association in the German-speaking countries and regions for research, teaching and professional work in natural language processing.

# Executive Board of the GSCL
The German Society for Language Technology and Computational Linguistics acts through its honorary mandate holders. The executive committee is responsible for all matters of the GSCL – e.g. convocation of the general meeting and implementation of its decisions, preparation of a budget, representation of GSCL to the outside – unless they are assigned by the statute to another body.

{% include board_en.html %}


### GSCL Advisory Board

The Advisory Board has the task of advising and assisting the Board in all matters relating to the work and tasks of the Association, including budgetary matters.


{% include boardadvisors_de.html %}



# [About us](#about-us)

Mission and Charter

The GSCL is the scientific association for research, teaching and professional work on natural language processing. It registered as a non-profit association, the charter can be found [here]({{ "/assets/satzung.pdf" | relative_url }}). The society represents the interests of its members and promotes cooperation between the members and their working fields, such as:

* automatic NL analysis and generation
* corpus engineering and corpus linguistics
* document processing and text technology
* information retrieval and knowledge management
* artificial intelligence and machine learning
* dictionaries and terminological databases
* machine translation
* human-machine communication
* multimedia and hypermedia
* tools for literary and linguistic research


The GSCL also supports [shared task initiatives](http://www.computerlinguistik.org/portal/portal.html?s=Shared%20Tasks) like [GermEval](https://germeval.github.io/), the cooperation with neighbouring disciplines (e.g., linguistics and semiotics, computer science and mathematics, psychology and cognitive science, data and information science, digital humanities), and keeps contact to the respective associations. There are international contacts with organisations like the [Association for Computational Linguistics](https://www.aclweb.org/portal/) and the [European Association for Digital Humanities](https://eadh.org/) (EADH), formerly Association for Literary and Linguistic Computing (ALLC).



 {% include about.html %}
